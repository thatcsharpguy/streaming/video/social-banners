class SocialCard {
	text: string;
	color: string;
	icon: string;
	textColor: string;
	iconFamily: string;

	constructor(text: string, color: string, icon: string, textColor?: string, iconFamily?: string) {
		this.text = text;
		this.color = color;
		this.icon = icon;
		this.textColor = textColor || "#fff";
		this.iconFamily = iconFamily || "fab";
	}
}

export default SocialCard;
