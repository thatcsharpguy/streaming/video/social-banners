import {library} from '@fortawesome/fontawesome-svg-core';
import {fab} from '@fortawesome/free-brands-svg-icons';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {registerRoot} from 'remotion';
import {RemotionVideo} from './Video';

library.add(fas, fab);

registerRoot(RemotionVideo);
