import {Sequence, useCurrentFrame, useVideoConfig} from 'remotion';
import {Card} from './Card';
import SocialCard from './SocialCard';

export const SocialCards: React.FC = () => {
	const frame = useCurrentFrame();
	const videoConfig = useVideoConfig();

	const socialNetworks: SocialCard[] = [
		new SocialCard('@io_exception', '#1DA1F2', 'twitter'),
		new SocialCard('tcsg.dev/discord', '#7289DA', 'discord'),
		new SocialCard('@thatcsharpguy', '#000', 'tiktok', "#fff"),
		// new SocialCard('/thatcsharpguy', '#4267B2', 'facebook'),
		new SocialCard('/thatcsharpguy', '#c13584', 'instagram'),
		new SocialCard('/thatcsharpguy', '#ff0000', 'youtube'),
		new SocialCard('tcsg.dev/irl', '#ff0000', 'youtube'),
		new SocialCard('tcsg.dev/dona', "rgb(252, 211, 77)", 'dollar-sign', "#000", "fas"),
		new SocialCard('tcsg.dev/ask', "rgb(238, 17, 68)", 'question', "#fff", "fas"),
		new SocialCard('/thatcsharpguy', '#FC6D26', 'gitlab'),
		new SocialCard('/thatcsharpguy', '#333', 'github'),
	];

	const sequenceDuration = videoConfig.durationInFrames / socialNetworks.length;

	return (
		<div style={{flex: 1, backgroundColor: 'white'}}>
			<div>
				{socialNetworks.map((socialNetwork, idx) => {
					return (
						<Sequence
							from={sequenceDuration * idx}
							durationInFrames={sequenceDuration}
						>
							<Card
								currentCard={socialNetwork}
								nextCard={socialNetworks[(idx + 1) % socialNetworks.length]}
								cardDuration={sequenceDuration}
							/>
						</Sequence>
					);
				})}
			</div>
		</div>
	);
};
