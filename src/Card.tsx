import {CSSProperties} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {interpolate, useCurrentFrame, useVideoConfig} from 'remotion';
import './Card.css';
import SocialCard from './SocialCard';

export const Card: React.FC<{
	cardDuration: number;
	currentCard: SocialCard;
	nextCard: SocialCard;
}> = ({currentCard, nextCard, cardDuration}) => {
	const frame = useCurrentFrame();
	const videoConfig = useVideoConfig();

	const cardWidth = videoConfig.width;
	const cardHeight = videoConfig.height;

	const transitionTime = cardDuration * 0.8;

	let topCardWidth = 0;
	if (frame > transitionTime) {
		topCardWidth = interpolate(
			frame,
			[transitionTime, cardDuration],
			[0, cardWidth]
		);
	}

	const currentIcon =  currentCard.iconFamily + ' fa-' + currentCard.icon;
	const nextIcon =  nextCard.iconFamily +' fa-' + nextCard.icon;

	const labelCss: CSSProperties = {
		textAlign: 'center',
		whiteSpace: 'nowrap',
		marginTop: videoConfig.height / 4,
		width: cardWidth,
		fontFamily: "'Open Sans', sans-serif",
	};

	const cardCss: CSSProperties = {
		color: 'white',
		position: 'absolute',
		overflow: 'hidden',
		width: cardWidth,
		height: cardHeight,
		fontSize: cardHeight / 2.5,
		left: 0,
	};

	const iconCss: CSSProperties = {
		paddingRight: cardHeight / 10,
	};

	return (
		<>
			<div
				style={{
					...cardCss,
					width: cardWidth,
					backgroundColor: currentCard.color,
					color: currentCard.textColor,
				}}
			>
				<div style={labelCss}>
					<FontAwesomeIcon icon={[currentCard.iconFamily, currentCard.icon]} />&nbsp;<span>{currentCard.text}</span>
				</div>
			</div>
			<div
				style={{
					...cardCss,
					width: topCardWidth,
					backgroundColor: nextCard.color,
					color: nextCard.textColor,
				}}
			>
				<div style={labelCss}>
					<FontAwesomeIcon icon={[nextCard.iconFamily, nextCard.icon]} />&nbsp;<span>{nextCard.text}</span>
				</div>
			</div>
		</>
	);
};
