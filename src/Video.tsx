import {Composition} from 'remotion';
import {SocialCards} from './SocialCards';

export const RemotionVideo: React.FC = () => {
	return (
		<>
			<Composition
				id="SocialCards"
				component={SocialCards}
				durationInFrames={1700}
				fps={30}
				width={600}
				height={86}
				defaultProps={{
					titleText: 'Welcome to Remotion',
					titleColor: 'black',
				}}
			/>
		</>
	);
};
